package com.gitlab.prototypeg.android

import android.os.Handler
import android.os.Looper
import android.widget.Toast

import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.preference.PreferenceManager

import com.gitlab.prototypeg.data.Language
import com.gitlab.prototypeg.doll.DollInfo
import com.gitlab.prototypeg.event.*

import org.apache.commons.lang3.StringUtils

class EventListener(private val context: PrototypeG) : java.util.EventListener {

    fun onDollBuild(dollBuildEvent: DollBuildEvent) {
        val dollInfo = DollInfo[dollBuildEvent.dollNumber]
        NotificationManagerCompat.from(context).notify(
                context.newNotificationId(),
                NotificationCompat.Builder(context, PrototypeG.BUILD)
                        .setSubText(context.getString(R.string.doll_build))
                        .setContentTitle(
                                dollInfo.getName(
                                        toLanguage(
                                                PreferenceManager.getDefaultSharedPreferences(context)
                                                        .getString(context.getString(R.string.language), "NONE")!!
                                        )
                                )
                        )
                        .setContentText(StringUtils.repeat("★", dollInfo.rank!!.toInt()))
                        .setSmallIcon(android.R.drawable.ic_dialog_info)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .build()
        )
    }

    fun onDataPacketDecodeFailed(event: DecodeFailedEvent) {
        event.throwable.printStackTrace()
        Handler(Looper.getMainLooper()).post { Toast.makeText(context, R.string.key_exchange_failed, Toast.LENGTH_LONG).show() }
    }

	fun onDollGet(event: DollGetEvent) {
		if (event.doll.info.rank!! != 1.toByte() && event.doll.info.rank!! < 4) {
			return
		}
		NotificationManagerCompat.from(context).notify(
				context.newNotificationId(),
				NotificationCompat.Builder(context, PrototypeG.BUILD)
						.setSubText(context.getString(R.string.doll_build))
						.setContentTitle(
								event.doll.info.getName(
										toLanguage(
												PreferenceManager.getDefaultSharedPreferences(context)
														.getString(context.getString(R.string.language), "NONE")!!
										)
								)
						)
						.setContentText(StringUtils.repeat("★", event.doll.info.rank!!.toInt()))
						.setSmallIcon(android.R.drawable.ic_dialog_info)
						.setPriority(NotificationCompat.PRIORITY_DEFAULT)
						.build()
		)
	}

	fun onMissionWin(missionWinEvent: MissionWinEvent) {
		NotificationManagerCompat.from(context).notify(
				context.newNotificationId(),
				NotificationCompat.Builder(context, PrototypeG.BATTLE_FINISH)
						.setContentTitle(context.getString(R.string.battle_finish))
						.setSmallIcon(android.R.drawable.ic_dialog_info)
						.setPriority(NotificationCompat.PRIORITY_DEFAULT)
						.build()
		)
	}

	fun onFriendVisit(friendVisitEvent: FriendVisitEvent) {
		Handler(Looper.getMainLooper()).post {
			Toast.makeText(context, context.getString(R.string.build_coin_count) + ": " + friendVisitEvent.friendVisitResponse.buildCoinCount, Toast.LENGTH_LONG).show()
		}
	}

    private fun toLanguage(language: String): Language {
        return when (language) {
            "CN" -> Language.CN
            "KR" -> Language.KR
            "EN" -> Language.EN
            "JP" -> Language.JP
            else -> Language.NONE
        }
    }
}
